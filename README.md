# README #

This is example of jquery script using instadoor.online API to generate post feed for your site.

This script need only jquery >=1.1.12. 

### Usage ###

//define api endpoints
var baseurl = 'https://instadoor.online/';
var api = 'api/tag';
```
//define request params
var data = {
	tag: "love",
	token: "YOUR_TOKEN",
	count: 10
}
$.get(baseurl+api,data,function(result){
	console.log('result',result);
	//see the output and create your template
	//example render:
	$.each(result.posts,function(i,el){
		$('#postfeed').append('<div><img src="'+el.img+'" alt="'+el.caption+'" /><div>');
	})
});


```

### Cross domain ###

Instadoor.online accepst both http/https connections. However it's private, and your domain will be added to allowed crossdomains once your api request is aprooved.

### Beta ###

Our database and API is still in beta and provided as is.